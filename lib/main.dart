import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Layout',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: const MyHomePage(title: 'First Layout'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final myController_user = TextEditingController();
  final myController_pwd = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController_user.dispose();
    myController_pwd.dispose();
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(

        color: Colors.redAccent,
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(

            children: [

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CircleAvatar(
                      radius: 80,
                      backgroundImage: AssetImage("assets/images/dog.jpeg"),
                    ),
                  ],
                ),
              ),
              Icon(Icons.insert_photo ,size: 63, color: Colors.white,),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    'ลงทะเบียน',
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        ),
                  ),
                  Text(
                    'ลืมรหัส',
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        ),
                  ),

                ],
              ),
              Padding(
                padding: const EdgeInsets.all(30),
                child: Text(
                  "This is Column",
                  style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, bottom: 30),
                child: TextField(
                  controller: myController_user,
                  decoration: InputDecoration(
                    hintText: 'Username',
                    fillColor: Colors.white,
                    filled: true,

                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, bottom: 30),
                child: TextField(
                  controller: myController_pwd,
                  decoration: InputDecoration(
                    hintText: 'Password',
                    fillColor: Colors.white,
                    filled: true,

                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(

                    style: ElevatedButton.styleFrom(
                      primary: Colors.white, //สี
                      onPrimary: Colors.red,
                      padding: EdgeInsets.symmetric(
                        horizontal: 30,
                        vertical: 15,


                      ),
                      textStyle: TextStyle(fontSize: 20,fontWeight:FontWeight.bold),


                    ),
                    onPressed: (){
                      myController_user.clear();
                      myController_pwd.clear();
                    },
                    child: Text("Cancel"),
                  ),
                  ElevatedButton(

                      style: ElevatedButton.styleFrom(
                        primary: Colors.white, //สี
                        onPrimary: Colors.red,
                        padding: EdgeInsets.symmetric(
                          horizontal: 30,
                          vertical: 15,
                        ),
                        textStyle: TextStyle(fontSize: 20,fontWeight:FontWeight.bold),
                      ),
                      onPressed: () =>displayToast(),
                      // print("Hello Login!!!"),
                      child: Text("Login")),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }//end build
  void displayToast(){
    String  username =myController_user.text;
    String  password =myController_pwd.text;

    Fluttertoast.showToast(msg:
    "You Username: $username \n  You Password: $password ",
      toastLength: Toast.LENGTH_SHORT,
    );
  }
}//end class _myhomepage